package me.cheracc.onedimension.modules;

import me.cheracc.onedimension.utilities.ConfigSetting;
import me.cheracc.onedimension.utilities.ConfigSettings;
import me.cheracc.onedimension.utilities.Logr;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.WanderingTrader;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.VillagerAcquireTradeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class VillagerTrades implements Listener, ConfigSettings {
    private final Logr logr;
    @ConfigSetting
    protected boolean enabled = true;
    @ConfigSetting
    protected boolean reverseNetherWartTrade = true;
    @ConfigSetting
    protected boolean addChorusFlowerTradeToFarmer = true;
    @ConfigSetting
    protected double probabilityOfFarmerGettingChorusFlowerTrade = 0.1;
    @ConfigSetting
    protected boolean addEndStoneTradeToMasons = true;
    @ConfigSetting
    protected double probabilityOfMasonGettingEndStoneTrade = 0.2;
    @ConfigSetting
    protected boolean addNetherrackTradeToMasons = true;
    @ConfigSetting
    protected double probabilityOfMasonGettingNetherrackTrade = 0.5;
    @ConfigSetting
    protected boolean addSoulSandTradeToClerics = true;
    @ConfigSetting
    protected double probabilityOfClericGettingSoulSandTrade = 0.2;
    @ConfigSetting
    protected boolean wanderingTradersCanSellNylium = true;
    @ConfigSetting
    protected double probabilityOfWanderingTraderSellingNylium = 0.2;

    public VillagerTrades(Logr logr) {
        this.logr = logr;
    }

    @EventHandler (ignoreCancelled = true)
    public void onWanderingTraderSpawns(EntitySpawnEvent event) {
        if (enabled && wanderingTradersCanSellNylium && event.getEntity() instanceof WanderingTrader trader) {
            double roll = ThreadLocalRandom.current().nextDouble();

            if (roll <= probabilityOfWanderingTraderSellingNylium) {
                boolean type = ThreadLocalRandom.current().nextBoolean();
                ItemStack emerald = new ItemStack(Material.EMERALD, 10);
                Material nyliumMat = type ? Material.CRIMSON_NYLIUM : Material.WARPED_NYLIUM;
                ItemStack nylium = new ItemStack(nyliumMat);
                MerchantRecipe nyliumTrade = new MerchantRecipe(nylium, 0, 3, true, 0, 1);
                nyliumTrade.addIngredient(emerald);

                List<MerchantRecipe> trades = new ArrayList<>(trader.getRecipes());
                trades.add(nyliumTrade);
                trader.setRecipes(trades);

                Location l = event.getEntity().getLocation();
                logr.debug("Added %s trade on wandering trader at (%.0f, %.0f, %.0f) [%.4f < %.4f] [%s]",
                        nyliumMat.name().toLowerCase(),
                        l.getX(), l.getY(), l.getZ(),
                        roll, probabilityOfMasonGettingEndStoneTrade,
                        event.getEntity().getUniqueId());
            }
        }
    }

    @EventHandler
    public void replaceTrades(VillagerAcquireTradeEvent event) {
        if (!enabled)
            return;

        MerchantRecipe recipe = event.getRecipe();
        ItemStack firstIngredient = recipe.getIngredients().get(0);
        double roll = ThreadLocalRandom.current().nextDouble();
        ItemStack emerald = new ItemStack(Material.EMERALD);

        if (firstIngredient == null)
            return;

        if (addSoulSandTradeToClerics && firstIngredient.getType() == Material.RABBIT_FOOT) {
            if (roll <= probabilityOfClericGettingSoulSandTrade) {
                ItemStack soulSand = new ItemStack(Material.SOUL_SAND, 2);
                MerchantRecipe soulSandTrade = new MerchantRecipe(soulSand, 0, 12, true, 10, 1);
                emerald.setAmount(8);
                List<ItemStack> ingredients = new ArrayList<>();
                ingredients.add(emerald);
                ingredients.add(new ItemStack(Material.SAND, 2));

                soulSandTrade.setIngredients(ingredients);
                event.setRecipe(soulSandTrade);

                Location l = event.getEntity().getLocation();
                logr.debug("Added soul sand trade on villager at (%.0f, %.0f, %.0f) [%.4f < %.4f] [%s]", l.getX(), l.getY(), l.getZ(), roll, probabilityOfMasonGettingEndStoneTrade, event.getEntity().getUniqueId());
            }
        }

        if (addNetherrackTradeToMasons && recipe.getResult().getType() == Material.CHISELED_STONE_BRICKS) {
            if (roll <= probabilityOfMasonGettingNetherrackTrade) {
                ItemStack netherrack = new ItemStack(Material.NETHERRACK, 4);
                MerchantRecipe netherrackTrade = new MerchantRecipe(netherrack, 0, 12, true, 15, 1);
                netherrackTrade.setIngredients(Collections.singletonList(emerald));
                event.setRecipe(netherrackTrade);

                Location l = event.getEntity().getLocation();
                logr.debug("Added netherrack trade on villager at (%.0f, %.0f, %.0f) [%.4f < %.4f] [%s]", l.getX(), l.getY(), l.getZ(), roll, probabilityOfMasonGettingEndStoneTrade, event.getEntity().getUniqueId());
            }
        }

        if (addEndStoneTradeToMasons && firstIngredient.getType() == Material.QUARTZ) {
            if (roll <= probabilityOfMasonGettingEndStoneTrade) {
                ItemStack endStone = new ItemStack(Material.END_STONE);
                MerchantRecipe endStoneTrade = new MerchantRecipe(endStone, 0, 12, true, 15, 1);
                endStoneTrade.setIngredients(Collections.singletonList(emerald));
                event.setRecipe(endStoneTrade);

                Location l = event.getEntity().getLocation();
                logr.debug("Added end stone trade on villager at (%.0f, %.0f, %.0f) [%.4f < %.4f] [%s]", l.getX(), l.getY(), l.getZ(), roll, probabilityOfMasonGettingEndStoneTrade, event.getEntity().getUniqueId());
            }
        }

        if (addChorusFlowerTradeToFarmer && recipe.getResult().getType() == Material.GLISTERING_MELON_SLICE) {
            if (roll <= probabilityOfFarmerGettingChorusFlowerTrade) {
                MerchantRecipe chorusTrade = new MerchantRecipe(new ItemStack(Material.CHORUS_FLOWER), 0, 1, true, 12, 1);
                chorusTrade.setIngredients(Collections.singletonList(new ItemStack(Material.EMERALD, 32)));
                event.setRecipe(chorusTrade);

                Location l = event.getEntity().getLocation();
                logr.debug("Added chorus flower trade on villager at (%.0f, %.0f, %.0f) [%.4f < %.4f] [%s]", l.getX(), l.getY(), l.getZ(), roll, probabilityOfFarmerGettingChorusFlowerTrade, event.getEntity().getUniqueId());
            }
        }

        if (reverseNetherWartTrade && firstIngredient.getType() == Material.NETHER_WART) {
            int amount = firstIngredient.getAmount();
            ItemStack result = new ItemStack(Material.NETHER_WART);

            MerchantRecipe reversed = new MerchantRecipe(result, 0,
                    recipe.getMaxUses(),
                    recipe.hasExperienceReward(),
                    recipe.getVillagerExperience(),
                    recipe.getPriceMultiplier());
            reversed.setIngredients(Collections.singletonList(new ItemStack(Material.EMERALD, amount)));

            event.setRecipe(reversed);

            Location l = event.getEntity().getLocation();
            logr.debug("Reversed nether wart trade on villager at (%.0f, %.0f, %.0f) [%s]", l.getX(), l.getY(), l.getZ(), event.getEntity().getUniqueId());
        }
    }
}
