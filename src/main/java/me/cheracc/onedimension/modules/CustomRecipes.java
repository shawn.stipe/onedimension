package me.cheracc.onedimension.modules;

import me.cheracc.onedimension.utilities.ConfigSetting;
import me.cheracc.onedimension.utilities.ConfigSettings;
import me.cheracc.onedimension.utilities.Logr;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.Plugin;
import java.util.ArrayList;
import java.util.List;

public class CustomRecipes implements ConfigSettings {
    @ConfigSetting
    protected boolean enabled = true;
    @ConfigSetting
    protected List<String> elytraRecipe;
    @ConfigSetting
    protected List<String> ancientDebrisRecipe;
    @ConfigSetting
    protected List<String> quartzRecipe;
    @ConfigSetting
    protected List<String> soulSoilRecipe;
    @ConfigSetting
    protected List<String> blackstoneRecipe;
    private final Logr logr;
    private final Plugin plugin;

    public CustomRecipes(Logr logr, Plugin plugin) {
        this.logr = logr;
        this.plugin = plugin;
        elytraRecipe = defaultElytraRecipe();
        ancientDebrisRecipe = defaultDebrisRecipe();
        quartzRecipe = defaultQuartzRecipe();
        soulSoilRecipe = defaultSoulSoilRecipe();
        blackstoneRecipe = defaultBlackstoneRecipe();
    }

    private List<String> defaultBlackstoneRecipe() {
        List<String> recipe = new ArrayList<>();

        recipe.add("COBBLED_DEEPSLATE");
        recipe.add("COBBLED_DEEPSLATE");
        recipe.add("COBBLED_DEEPSLATE");
        recipe.add("COBBLED_DEEPSLATE");
        recipe.add("COAL_BLOCK");
        recipe.add("COBBLED_DEEPSLATE");
        recipe.add("COBBLED_DEEPSLATE");
        recipe.add("COBBLED_DEEPSLATE");
        recipe.add("COBBLED_DEEPSLATE");

        return recipe;
    }

    private List<String> defaultSoulSoilRecipe() {
        List<String> recipe = new ArrayList<>();

        recipe.add("SOUL_SAND");
        recipe.add("SOUL_SAND");
        recipe.add("SOUL_SAND");
        recipe.add("SOUL_SAND");
        recipe.add("ROTTEN_FLESH");
        recipe.add("DIRT");
        recipe.add("DIRT");
        recipe.add("DIRT");
        recipe.add("DIRT");

        return recipe;
    }

    private List<String> defaultQuartzRecipe() {
        List<String> recipe = new ArrayList<>();

        recipe.add("DIORITE");
        recipe.add("DIORITE");
        recipe.add("DIORITE");
        recipe.add("DIORITE");
        recipe.add("DIORITE");
        recipe.add("DIORITE");
        recipe.add("DIORITE");
        recipe.add("DIORITE");
        recipe.add("DIORITE");

        return recipe;
    }

    private List<String> defaultDebrisRecipe() {
        List<String> recipe = new ArrayList<>();

        recipe.add("DEEPSLATE_DIAMOND_ORE");
        recipe.add("DEEPSLATE_DIAMOND_ORE");
        recipe.add("DEEPSLATE_DIAMOND_ORE");
        recipe.add("CRYING_OBSIDIAN");
        recipe.add("DIAMOND_BLOCK");
        recipe.add("CRYING_OBSIDIAN");
        recipe.add("DEEPSLATE_DIAMOND_ORE");
        recipe.add("DEEPSLATE_DIAMOND_ORE");
        recipe.add("DEEPSLATE_DIAMOND_ORE");

        return recipe;
    }

    private List<String> defaultElytraRecipe() {
        List<String> recipe = new ArrayList<>();

        recipe.add("NETHERITE_INGOT");
        recipe.add("NETHER_STAR");
        recipe.add("NETHERITE_INGOT");
        recipe.add("NETHERITE_INGOT");
        recipe.add("BLANK");
        recipe.add("NETHERITE_INGOT");
        recipe.add("NETHERITE_INGOT");
        recipe.add("BLANK");
        recipe.add("NETHERITE_INGOT");

        return recipe;
    }

    public void loadRecipes() {
        if (enabled) {
            NamespacedKey key = new NamespacedKey(plugin, "elytra");
            loadRecipe(new ItemStack(Material.ELYTRA), elytraRecipe, key);
            NamespacedKey debrisKey = new NamespacedKey(plugin, "ancient_debris");
            loadRecipe(new ItemStack(Material.ANCIENT_DEBRIS), ancientDebrisRecipe, debrisKey);
            NamespacedKey quartzKey = new NamespacedKey(plugin, "quartz");
            loadRecipe(new ItemStack(Material.QUARTZ), quartzRecipe, quartzKey);
            NamespacedKey soulSoilKey = new NamespacedKey(plugin, "soul_soil");
            loadRecipe(new ItemStack(Material.SOUL_SOIL, 8), soulSoilRecipe, soulSoilKey);
            NamespacedKey blackstoneKey = new NamespacedKey(plugin, "blackstone");
            loadRecipe(new ItemStack(Material.BLACKSTONE, 9), blackstoneRecipe, blackstoneKey);
            logr.debug("Loaded Custom Recipes");
        }
    }

    private void loadRecipe(ItemStack item, List<String> recipe, NamespacedKey key) {
        ShapedRecipe shapedRecipe = new ShapedRecipe(key, item);

        shapedRecipe.shape("012", "345", "678");

        for (int i = 0; i < recipe.size(); i++) {
            String s = recipe.get(i).toUpperCase();
            String symbol = Integer.toString(i);
            char c = symbol.charAt(0);

            if (s.equals("BLANK") || s.equals("AIR") || s.equals(" ")) {
                shapedRecipe.setIngredient(c, new ItemStack(Material.AIR));
                continue;
            }
            Material m = Material.valueOf(s);
            ItemStack mat = new ItemStack(m);

            shapedRecipe.setIngredient(c, mat);
        }
        plugin.getServer().removeRecipe(key);
        plugin.getServer().addRecipe(shapedRecipe);
    }

}
