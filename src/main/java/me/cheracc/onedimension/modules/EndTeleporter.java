package me.cheracc.onedimension.modules;

import me.cheracc.onedimension.utilities.ConfigSetting;
import me.cheracc.onedimension.utilities.ConfigSettings;
import me.cheracc.onedimension.utilities.Logr;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;

public class EndTeleporter implements ConfigSettings, Listener {
    @ConfigSetting
    protected boolean enabled = true;
    @ConfigSetting
    protected int teleportHeight = 400;
    private final Logr logr;

    public EndTeleporter(Logr logr) {
        this.logr = logr;
    }

    @EventHandler
    public void onEndPortalTeleport(EntityPortalEnterEvent event) {
        if (enabled && event.getLocation().getBlock().getType() == Material.END_PORTAL) {

            if (event.getEntity() instanceof Player p) {
                Location teleportLocation = p.getLocation().clone();

                teleportLocation.setY(teleportHeight);
                p.teleport(teleportLocation);
            }
        }
    }
}
