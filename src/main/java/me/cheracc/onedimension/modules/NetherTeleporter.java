package me.cheracc.onedimension.modules;

import me.cheracc.onedimension.utilities.ConfigSetting;
import me.cheracc.onedimension.utilities.ConfigSettings;
import me.cheracc.onedimension.utilities.Logr;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class NetherTeleporter implements ConfigSettings, Listener {
    @ConfigSetting
    protected boolean enabled = true;
    @ConfigSetting
    protected boolean destroyPortalOnUse = true;
    @ConfigSetting
    protected boolean onlyPortalBuilderMayUse = true;
    @ConfigSetting
    protected int maxTeleportDistance = 1000;
    private final Plugin plugin;
    private final Logr logr;
    private final Map<String, Long> attempts = new HashMap<>();

    public NetherTeleporter(Plugin plugin, Logr logr) {
        this.plugin = plugin;
        this.logr = logr;
    }

    @EventHandler
    public void onTeleport(EntityPortalEnterEvent event) {
        if (enabled && event.getLocation().getBlock().getType() == Material.NETHER_PORTAL) {
            if (event.getEntity() instanceof Player p) {
                clearAttempts();
                if (attempts.containsKey(p.getName()))
                    return;

                attempts.put(p.getName(), System.currentTimeMillis());

                if (onlyPortalBuilderMayUse) {
                    boolean found = false;

                    for (BlockState b : getPortalBlocks(p.getLocation()))
                        if (isCreator(b, p))
                            found = true;

                    if (!found) {
                        p.sendMessage("That is not your portal");
                        return;
                    }
                }

                if (destroyPortalOnUse) {
                    destroyPortal(p.getLocation());
                }

                p.teleport(getRandomLocation(p.getLocation()));
            }
        }
    }

    @EventHandler
    public void onPortalCreation(PortalCreateEvent event) {
        if (enabled && event.getEntity() instanceof Player p)
            for (BlockState b : event.getBlocks()) {
                setPortalCreator(b, p);
                b.update(true);
            }
    }

    /**
     * Removes all recent attempts to use a nether portal
     */
    private void clearAttempts() {
        List<String> toRemove = new ArrayList<>();

        for (Map.Entry<String, Long> e : attempts.entrySet()) {
            if (System.currentTimeMillis() - e.getValue() >= 5000)
                toRemove.add(e.getKey());
        }
        toRemove.forEach(attempts::remove);
    }

    /**
     * Destroys a nether portal by locating all adjacent obsidian and replacing it with crying obsidian (rare) or black concrete powder
     * @param location the location to look for the portal blocks
     */
    private void destroyPortal(Location location) {
        for (BlockState b : getPortalBlocks(location)) {
            if (b.hasMetadata("portal creator")) {
                for (BlockFace face: BlockFace.values()) {
                    Block check = b.getBlock().getRelative(face);
                    if (check.getType() == Material.OBSIDIAN) {
                        if (ThreadLocalRandom.current().nextDouble() >= 0.95) {
                            check.setType(Material.CRYING_OBSIDIAN);
                        } else {
                            check.setType(Material.BLACK_CONCRETE_POWDER, true);
                        }
                    }
                }
                b.setType(Material.AIR);
                b.update(true);
            }
        }
        location.getWorld().playSound(location, Sound.BLOCK_GLASS_BREAK, 1F, 1F);
    }

    /**
     * Locates nether portal blocks within 10 blocks of the given location
     * @param location the location to look for portal blocks
     * @return a list of blockstates representing the found portal blocks, or an empty list if none were found
     */
    private List<BlockState> getPortalBlocks(Location location) {
        List<BlockState> portalBlocks = new ArrayList<>();

        for (int x = -10; x <= 10; x++)
            for (int y = -10; y <= 10; y++)
                for (int z = -10; z <= 10; z++) {
                    Block b = location.clone().add(x, y, z).getBlock();
                    if (b.getType() == Material.NETHER_PORTAL)
                        portalBlocks.add(b.getState());
                }

        return portalBlocks;
    }

    /**
     * @param block the block to check
     * @param player the player to check
     * @return true if the portal block was created due to the player
     */
    private boolean isCreator(BlockState block, Player player) {
        if (block.hasMetadata("portal creator")) {
            for (MetadataValue v : block.getMetadata("portal creator")) {
                if (v.getOwningPlugin() != null && v.getOwningPlugin().equals(plugin)) {
                    String owner = v.asString();
                    if (player.getName().equals(owner))
                        return true;
                }
            }
        }
        return false;
    }

    /**
     * Gives a block a metadata tag identifying the player as the portal creator
     * @param block the block to set the creator of
     * @param player the player to set as the creator
     */
    private void setPortalCreator(BlockState block, Player player) {
        block.setMetadata("portal creator", new FixedMetadataValue(plugin, player.getName()));
    }

    /**
     * @param origin the center location to derive random location from
     * @return a random location up to the maxTeleportDistance away from the origin
     */
    private Location getRandomLocation(Location origin) {
        Random rand = ThreadLocalRandom.current();
        int randX = (int) origin.getX() + rand.nextInt(-maxTeleportDistance, maxTeleportDistance);
        int randZ = (int) origin.getY() + rand.nextInt(-maxTeleportDistance, maxTeleportDistance);

        return origin.getWorld().getHighestBlockAt(randX, randZ).getLocation();
    }

}
