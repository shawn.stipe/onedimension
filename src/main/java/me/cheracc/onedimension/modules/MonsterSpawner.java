package me.cheracc.onedimension.modules;

import me.cheracc.onedimension.utilities.ConfigSetting;
import me.cheracc.onedimension.utilities.ConfigSettings;
import me.cheracc.onedimension.utilities.Logr;
import org.bukkit.Location;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.potion.PotionEffectTypeWrapper;

import java.util.concurrent.ThreadLocalRandom;

public class MonsterSpawner implements Listener, ConfigSettings {
    private final Logr logr;
    @ConfigSetting
    protected boolean enabled = true;
    @ConfigSetting
    protected boolean convertSkeletonToBlazeOnFireDamage = true;
    @ConfigSetting
    protected double probabilityOfConvertingToBlaze = 0.01;
    @ConfigSetting
    protected boolean convertSlimeToMagmaCubeOnFireDamage = true;
    @ConfigSetting
    protected double probabilityOfConvertingToMagmaCube = 0.05;
    @ConfigSetting
    protected boolean witherSkeletonsRandomlyReplaceSkeletons = true;
    @ConfigSetting
    protected double probabilityOfWitherSkeleton = 0.002;
    @ConfigSetting
    protected boolean allowWitherSkeletonsToSpawnFromSkeletonSpawners = false;
    @ConfigSetting
    protected boolean spawnShulkersWhereEndermenPlaceBlocks = true;
    @ConfigSetting
    protected double probabilityOfPlacingShulker = 0.05;
    @ConfigSetting
    protected boolean hoglinsRandomlySpawnWhenBreedingPigs = true;
    @ConfigSetting
    protected double probabilityOfBreedingAHoglin = 0.1;
    @ConfigSetting
    protected boolean spawnGhastsFromPhantomsStruckByTippedArrows = true;
    @ConfigSetting
    protected boolean buffSpawnedGhasts = true;

    public MonsterSpawner(Logr logr) {
        this.logr = logr;
    }

    @EventHandler
    public void spawnGhast(EntityPotionEffectEvent event) {
        if (enabled && spawnGhastsFromPhantomsStruckByTippedArrows) {
            if (event.getEntity() instanceof Phantom phantom && event.getCause() == EntityPotionEffectEvent.Cause.ARROW) {
                Location loc = phantom.getLocation();
                phantom.remove();
                Ghast ghast = (Ghast) loc.getWorld().spawnEntity(loc, EntityType.GHAST);
                ghast.setPersistent(true);

                if (buffSpawnedGhasts) {
                    ghast.addPotionEffect(PotionEffectTypeWrapper.INCREASE_DAMAGE.createEffect(9999, 1));
                    ghast.addPotionEffect(PotionEffectTypeWrapper.DAMAGE_RESISTANCE.createEffect(9999, 1));
                }
                logr.debug("Spawned ghast in place of phantom at (%.0f, %.0f, %.0f)", loc.getX(), loc.getY(), loc.getZ());
            }
        }
    }

    @EventHandler
    public void spawnHoglins(EntityBreedEvent event) {
        if (enabled && hoglinsRandomlySpawnWhenBreedingPigs) {
            if (event.getMother() instanceof Pig p) {
                double roll = ThreadLocalRandom.current().nextDouble();

                if (roll <= probabilityOfBreedingAHoglin) {
                    Location loc = p.getLocation();
                    Hoglin hoglin = (Hoglin) loc.getWorld().spawnEntity(loc, EntityType.HOGLIN);
                    hoglin.setBaby();
                    hoglin.setPersistent(true);
                    logr.debug("Spawned hoglin in place of pig at (%.0f, %.0f, %.0f). [%.4f < %.4f]", loc.getX(), loc.getY(), loc.getZ(), roll, probabilityOfBreedingAHoglin);
                }
            }
        }

    }

    @EventHandler
    public void spawnHoglins(EntityTransformEvent event) {
        if (enabled && hoglinsRandomlySpawnWhenBreedingPigs) {
            if (event.getEntity() instanceof Pig pig1 && event.getTransformedEntity() instanceof Pig pig2) {
                if (!pig1.isAdult() && pig2.isAdult()) {
                    double roll = ThreadLocalRandom.current().nextDouble();

                    if (roll <= probabilityOfBreedingAHoglin) {
                        event.setCancelled(true);
                        Location loc = pig1.getLocation();

                        pig1.remove();
                        Hoglin hoglin = (Hoglin) loc.getWorld().spawnEntity(loc, EntityType.HOGLIN);
                        hoglin.setAdult();
                        hoglin.setPersistent(true);
                        logr.debug("Spawned hoglin in place of pig at (%.0f, %.0f, %.0f). [%.4f < %.4f]", loc.getX(), loc.getY(), loc.getZ(), roll, probabilityOfBreedingAHoglin);
                    }
                }
            }
        }
    }

    @EventHandler
    public void placeShulkers(EntityChangeBlockEvent event) {
        if (enabled && spawnShulkersWhereEndermenPlaceBlocks) {
            if (event.getEntity() instanceof Enderman e && !event.getTo().isAir()) {
                double roll = ThreadLocalRandom.current().nextDouble();

                if (roll <= probabilityOfPlacingShulker) {
                    event.setCancelled(true);
                    Location loc = event.getBlock().getLocation();

                    Shulker shulker = (Shulker) loc.getWorld().spawnEntity(loc, EntityType.SHULKER);
                    e.setCarriedBlock(null);
                    shulker.setPersistent(true);
                    logr.debug("Spawned shulker in place of %s at (%.0f, %.0f, %.0f). [%.4f < %.4f]",
                            event.getTo().name(), loc.getX(), loc.getY(), loc.getZ(), roll, probabilityOfPlacingShulker);
                }
            }
        }
    }

    @EventHandler (ignoreCancelled = true)
    public void onSpawn(CreatureSpawnEvent event) {
        if (enabled) {
            double roll = ThreadLocalRandom.current().nextDouble();

            if (witherSkeletonsRandomlyReplaceSkeletons && event.getEntity().getType() == EntityType.SKELETON) {
                if (!allowWitherSkeletonsToSpawnFromSkeletonSpawners && event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.SPAWNER)
                    return;

                if (roll <= probabilityOfWitherSkeleton) {
                    Location loc = event.getLocation();

                    event.setCancelled(true);
                    loc.getWorld().spawnEntity(loc, EntityType.WITHER_SKELETON);
                    logr.debug("Spawned wither skeleton in place of skeleton at (%.0f, %.0f, %.0f). [%.4f < %.4f]", loc.getX(), loc.getY(), loc.getZ(), roll, probabilityOfWitherSkeleton);
                }
            }
        }
    }

    @EventHandler (ignoreCancelled = true)
    public void onFireDamage(EntityCombustEvent event) {
        if (enabled) {
            double roll = ThreadLocalRandom.current().nextDouble();

            if (convertSkeletonToBlazeOnFireDamage && event.getEntity() instanceof Skeleton skeleton) {
                if (roll <= probabilityOfConvertingToBlaze) {
                    Location loc = skeleton.getLocation();
                    skeleton.remove();

                    loc.getWorld().spawnEntity(loc, EntityType.BLAZE);
                    logr.log("Converted skeleton to blaze at (%.0f, %.0f, %.0f). [%.4f < %.4f]", loc.getX(), loc.getY(), loc.getZ(), roll, probabilityOfConvertingToBlaze);
                }
            }
            if (convertSlimeToMagmaCubeOnFireDamage && event.getEntity() instanceof Slime slime) {
                if (roll <= probabilityOfConvertingToMagmaCube) {
                    Location loc = slime.getLocation();
                    int size = slime.getSize();
                    slime.remove();

                    MagmaCube cube = (MagmaCube) loc.getWorld().spawnEntity(loc, EntityType.MAGMA_CUBE);
                    cube.setSize(size);
                    logr.log("Converted slime to magma cube at (%.0f, %.0f, %.0f). [%.4f < %.4f]", loc.getX(), loc.getY(), loc.getZ(), roll, probabilityOfConvertingToMagmaCube);
                }
            }
        }
    }
}
