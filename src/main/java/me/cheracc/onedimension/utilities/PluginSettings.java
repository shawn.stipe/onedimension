package me.cheracc.onedimension.utilities;

import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.FileConfiguration;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PluginSettings implements ConfigSettings{
    private final List<Setting> settings;
    private final Logr logr;
    @ConfigSetting
    protected boolean disableNether = true;
    @ConfigSetting
    protected boolean disableEnd = true;

    public PluginSettings(Logr logr) {
        settings = new ArrayList<>();
        this.logr = logr;

        try {
            for (Field f : getClass().getDeclaredFields()) {
                if (f.isAnnotationPresent(ConfigSetting.class)) {
                    f.trySetAccessible();
                    Setting s = new Setting(f.getName(), f.get(this), this);
                    settings.add(s);
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public boolean isDisableNether() {
        return disableNether;
    }

    public boolean isDisableEnd() {
        return disableEnd;
    }

    public void attach(ConfigSettings object) throws IllegalAccessException {
        for (Field f : object.getClass().getDeclaredFields()) {
            if (f.isAnnotationPresent(ConfigSetting.class)) {
                f.trySetAccessible();
                Setting s = new Setting(object.getClass().getSimpleName() + "." + f.getName(), f.get(object), object);
                logr.debug("Adding setting: %s.%s, type: %s, value: %s", object.getClass().getSimpleName(),
                        f.getName(), f.getType().getName(), f.get(object).toString());
                settings.add(s);
            }
        }
    }

    public void writeSettingsToConfig(FileConfiguration config) {
        for (Setting s : settings) {
            config.set(s.getName(), s.getValue());
        }
    }

    public void loadFromConfig(FileConfiguration config) {
        for (Map.Entry<String, Object> s : config.getValues(true).entrySet()) {
            if (s.getValue() == null || s.getValue() instanceof MemorySection)
                continue;

            Setting setting = getByKey(s.getKey());
            if (setting.isNull()) {
                logr.debug("Unknown config option: %s", s);
                continue;
            }

            Object oldValue = setting.getValue();
            setting.setValue(s.getValue());
            if (!oldValue.equals(s.getValue()))
                logr.debug("set %s to %s (default is %s)", setting.getName(), setting.getValue().toString(), oldValue);
        }
    }

    private Setting getByKey(String deepKey) {
        for (Setting s : settings) {
            if (s.getName().equals(deepKey))
                return s;
        }
        return new Setting(deepKey, null, null);
    }

    private static class Setting {
        private final ConfigSettings linked;
        private final String name;
        private Object value;

        public Setting(String name, Object value, ConfigSettings linkedObject) {
            this.name = name;
            this.value = value;
            linked = linkedObject;
        }

        public void setValue(Object value) {
            this.value = value;

            if (linked == null)
                return;

            String[] split = name.split("\\.");
            String fieldName = split[split.length - 1];

            try {
                Field f = linked.getClass().getDeclaredField(fieldName);
                f.trySetAccessible();
                f.set(linked, value);
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        public String getName() {
            return name;
        }

        public boolean isNull() {
            return linked == null || value == null;
        }

        public Object getValue() {
            return value;
        }
    }
}
