package me.cheracc.onedimension;

import me.cheracc.onedimension.modules.*;
import me.cheracc.onedimension.utilities.ConfigSettings;
import me.cheracc.onedimension.utilities.Logr;
import me.cheracc.onedimension.utilities.PluginSettings;
import org.bukkit.World;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public final class OneDimension extends JavaPlugin {
    private PluginSettings settings;
    private Logr logr;

    @Override
    public void onEnable() {
        logr = new Logr(this);
        settings = new PluginSettings(logr);

        initialize(logr);
        initialize(new NetherTeleporter(this, logr));
        initialize(new EndTeleporter(logr));
        initialize(new MonsterSpawner(logr));
        CustomRecipes recipes = new CustomRecipes(logr, this);
        initialize(new VillagerTrades(logr));
        initialize(recipes);

        // do all module initializations first
        settings.loadFromConfig(getConfig());
        settings.writeSettingsToConfig(getConfig());
        saveConfig();
        recipes.loadRecipes();

        unloadWorlds();
    }

    /**
     * Initializes a module with the settings manager
     * @param module the plugin module that contains configuration settings
     */
    private void initialize(ConfigSettings module) {
        try {
            settings.attach(module);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (module instanceof Listener listener) {
            getServer().getPluginManager().registerEvents(listener, this);
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        settings.writeSettingsToConfig(getConfig());
        saveConfig();
    }

    /**
     * Unloads worlds that are disabled by the plugin
     */
    private void unloadWorlds() {
        List<World> toUnload = new ArrayList<>();

        for (World w : getServer().getWorlds()) {
            if (w.getEnvironment() == World.Environment.NETHER && settings.isDisableNether())
                toUnload.add(w);
            if (w.getEnvironment() == World.Environment.THE_END && settings.isDisableEnd())
                toUnload.add(w);
        }

        if (!toUnload.isEmpty()) {
            logr.log("End and/or Nether is disabled but these worlds are still being loaded. The plugin will unload them but you can save startup time and resources by setting allow-nether to false in server.properties and allow-end to false in bukkit.yml");
            toUnload.forEach(w -> getServer().unloadWorld(w, false));
        }

        logr.debug("Currently loaded worlds: %s", getServer().getWorlds().size());
    }
}
