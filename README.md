# OneDimension
A simple Paper plugin that removes the nether and end dimensions and provides end/nether materials through monster spawns, loot drops, crafting recipes, and villager trades. 

Intended for free/low-resource servers (like those running on Aternos, or free-but-limited VMs on Google/Oracle Cloud, etc.) or any server looking for a modified SMP experience.

## What it does

Almost all of these changes are configurable. By default, the plugin will:

- Disable the Nether
  - constructed nether portals instead teleport players to a random location within 1000 blocks (configurable)
  - constructed nether portals are destroyed when used (configurable - provides renewable crying obsidian)
  - only the portal creator can use it (configurable)
- Disable the End
  - when entering an end portal, the player is instead teleported to the same x/z coordinate, but at y=400 (configurable)
- Spawn Nether and End Monsters in the overworld
  - skeletons will rarely turn into blazes when taking fire damage
  - slimes will rarely turn into magma creams when taking fire damage
  - wither skeletons will very rarely spawn in place of skeletons (but not from spawners)
  - zoglins will randomly be bred when breeding pigs
  - endermen will occasionally place a shulker where they would have otherwise randomly placed the block they were holding
  - ghasts are spawned when a phantom is struck by a tipped or spectral arrow
- Add trades to villagers to provide some missing materials
  - masons have a chance of selling netherrack (1 emerald: 4 netherrack @ level 2) and end stone (1:1 @ level 4)
  - clerics nether wart trade is reversed (they sell one nether wart for 22 emeralds instead of the opposite)
  - clerics have a chance of selling soul sand (8 emeralds + 2 sand for 2 soul sand) as a level 3 trade
  - wandering traders can sell warped and crimson nylium
  - farmers have a chance of selling a chorus flower as one of their max-level trades
- Add custom recipes to provide some missing materials (the inputs to all of these are configurable!)
  - 6 Netherite Ingot + 1 Nether Star = Elytra: ![Elytra](images/elytrarecipe.png "Elytra Recipe")
  - 6 Deepslate Diamond Ores + 2 Crying Obsidian + 1 Diamond Block = 1 Ancient Debris: ![Ancient Debris](images/debrisrecipe.png "Ancient Debris Recipe")
  - 9 Diorite = 1 Nether Quartz: ![Nether Quartz](images/quartzrecipe.png "Nether Quartz Recipe")
  - 4 Soul Sand + 4 Dirt + 1 Rotten Flesh = 8 Soul Soil: ![Soul Soil](images/soulsoilrecipe.png "Soul Soil Recipe")
  - 8 Cobbled Deepslate + 1 Block of Coal = 9 Blackstone: ![Blackstone](images/blackstonerecipe.png "Blackstone Recipe")

## Installation

Just drop the .jar in your plugins/ directory on your 1.18+ Paper server. Since this plugin denies access to the nether and end dimensions, it is also wise to disable these from ever loading by setting:

- allow-nether: false (in server.properties)
- allow-end: false (in bukkit.yml)

The plugin will unload them if these are not set, but you can save startup time and resources by disabling them entirely. If you never plan to use these dimensions, you can then also safely delete the "world_nether" and "world_the_end" folders from your main server directory.

## Requirements/Prerequisites/Dependencies
None

## Configuration
All configuration options are in the config.yml file. See the Wiki for detailed instructions.

## Permissions/Commands
There are no permissions or commands.